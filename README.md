## Pacman & Ghost Animation

![Check it out](https://i.imgur.com/FhgJFUS.png)

[DEMO](https://natizyskunk.gitlab.io/Pacman-and-Ghost-Animation)

### Infos
- You can use the [editor](https://gitlab.com/Natizyskunk/Pacman-and-Ghost-Animation/edit/master/README.md) on this Gitlab repo to maintain the README markdown file.

- Whenever you commit to this repository, Gitlab Pages will run [Jekyll](https://jekyllrb.com) to rebuild the pages in your site, from the content in your HTML/CSS files.

### Support or Contact

- Having trouble with Pages? Check out our [documentation](https://gitlab.com/help/user/project/pages/index.md) or [contact support](https://about.gitlab.com/support) and we’ll help you sort it out.
